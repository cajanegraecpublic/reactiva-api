<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /*
        Tabla:
            puntos
        Campos:
            id     (int 11)[PK]
            nombre (varchar 100)
    */

	class Punto extends CI_Model
    {
        private $id;
        private $nombre;
        private $coord_x;
        private $coord_y;
        private $coord_z;

        function __construct()
        {
            parent::__construct();

            // Helpers
            $this->load->database();
        }

        ///////////////////////////////////
        // Getters
        ///////////////////////////////////
        public function get_id()
        {
            return $this->id;
        }
        public function get_nombre()
        {
            return $this->nombre;
        }
        public function get_coord_x()
        {
            return $this->coord_x;
        }
        public function get_coord_y()
        {
            return $this->coord_y;
        }
        public function get_coord_z()
        {
            return $this->coord_z;
        }

        ///////////////////////////////////
        // Setters
        ///////////////////////////////////
        public function set_id($id)
        {
            $this->id = $id;
        }
        public function set_nombre($nombre)
        {
            $this->nombre = $nombre;
        }
        public function set_coord_x($coord_x)
        {
            $this->coord_x = $coord_x;
        }
        public function set_coord_y($coord_y)
        {
            $this->coord_y = $coord_y;
        }
        public function set_coord_z($coord_z)
        {
            $this->coord_z = $coord_z;
        }

        ///////////////////////////////////
        // Métodos
        ///////////////////////////////////
        // Funcion para recuperar un punto de la DB
        public function fetch_by_id($punto_id)
        {   
            if (!is_null($punto_id)) {
                // Comprobamos si el id de punto existe en la DB
                if ($this->id_exists($punto_id)) {
                    // Obtentemos el punto de la DB
                    $punto_db = $this->db->get_where('puntos', array('id' => $punto_id))->last_row();

                    $this->id = $punto_db->id;
                    $this->nombre = $punto_db->nombre;

                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }

        // Funcion para comprobar si un id de juego existe en la DB
        public function id_exists($punto_id)
        {
            if (!is_null($punto_id)) {
                // Intentamos obtener el juego de la DB
                $punto_db = $this->db->get_where('puntos', array('id' => $punto_id))->last_row();

                if (!is_null($punto_db)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }
	}
?>