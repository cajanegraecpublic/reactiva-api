<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->model('Juego');
		$this->load->model('Punto');
		$this->load->library('Requests');
	}

	public function get_game_parameters ()
	{	
		// Inicializamos el array que se enviará de respuesta
		$response = array();

		// Recibimos el id del juego
		$game_id = $this->input->get('game');

		if (!empty($game_id)) {
			// Cargamos la librerí Requests y la inicializamos
			$this->load->library('Requests');
			Requests::register_autoloader();

			// Recuperamos el juego de la DB
			$game = new Juego();

			// Validamos que se haya podido recuperar el juego de la DB
			if ($game->fetch_by_id($game_id)) {
				// Consultamos las coordenadas de cada punto del juego
				foreach ($game->get_puntos() as $point) {
					$request = Requests::get(base_url('web_api/get_point_info?point=' . $point->get_id()));

					// Validamos si se pudo hacer el request
					if ($request->success) {
						$request_arr = json_decode($request->body, False);

						// Validamos si hubo algun error con el request
						if (!isset($request_arr->error)) {
							$response[$point->get_nombre()] = array(
								'x' => $request_arr->Position->x,
								'y' => $request_arr->Position->y,
								'z' => $request_arr->Position->z
							);
						} else {
							$response[$point->get_nombre()] = array(
								$point->get_nombre() => $request_arr->mensaje
							);
						}
					} else {
						$response[$point->get_nombre()] = array(
							'error' => 404,
							'mensaje' => 'no hay conexión con el servidor'
						);
					}
				}				
			} else {
				$response['error'] = 404;
				$response['mensaje'] = 'identificador de juego no válido';
			}

		} else {
			$response['error'] = 404;
			$response['mensaje'] = 'no envió identificador de juego';
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	// Metodo provisional para simular la peticion de coordenadas de algun punto
	public function get_point_info ()
	{
		// Recibimos el nombre del punto
		$point = $this->input->get('point');

		// Verificamos si el punto existe en la DB
		$point_instance = new Punto();
		if ($point_instance->id_exists($point)) {
			$result_arr = array(
				'TrackingState' => 'Inferred',
				'Position' => array(
					'x' => 0.7385729,
					'y' => -0.5850655,
					'z' => 2.549776
				),
				'BoneOrientations' => array(
					'EndJoint' => 'HandRight',
					'StartJoint' => 'WristRight',
					'AbsoluteRotation' => array(
						'Quaternion' => array(
							'w' => -0.4562912,
							'x' => -0.3868681,
							'y' => 0.2524863,
							'z' => 0.7605144
						)
					)
				)
			);
		} else {
			$result_arr = array(
				'error' => 404,
				'mensaje' => 'no hay lectura del kinect'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($result_arr);
	}
}