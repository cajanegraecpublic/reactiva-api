<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /*
        Tabla:
            juegos
        Campos:
            id     (int 11)[PK]
            nombre (varchar 100)
    */

	class Juego extends CI_Model
    {
        private $id;
        private $nombre;
        private $puntos;

        function __construct()
        {
            parent::__construct();

            // Helpers
            $this->load->database();
            $this->load->model('Punto');
        }

        ///////////////////////////////////
        // Getters
        ///////////////////////////////////
        public function get_id()
        {
            return $this->id;
        }
        public function get_nombre()
        {
            return $this->nombre;
        }
        public function get_puntos()
        {
            return $this->puntos;
        }

        ///////////////////////////////////
        // Setters
        ///////////////////////////////////
        public function set_id($id)
        {
            $this->id = $id;
        }
        public function set_nombre($nombre)
        {
            $this->nombre = $nombre;
        }

        ///////////////////////////////////
        // Métodos
        ///////////////////////////////////
        // Funcion para recuperar un juego de la DB
        public function fetch_by_id($juego_id)
        {   
            if (!is_null($juego_id)) {
                // Comprobamos si el id de juego existe en la DB
                if ($this->id_exists($juego_id)) {
                    // Obtentemos el juego de la DB
                    $juego_db = $this->db->get_where('juegos', array('id' => $juego_id))->last_row();

                    $this->id = $juego_db->id;
                    $this->nombre = $juego_db->nombre;

                    // Obtenemos los puntos del juego de la DB
                    $puntos_db = $this->db->get_where('juegos_puntos', array('id_juego' => $juego_id))->result();
                    $this->puntos = array();
                    foreach ($puntos_db as $row) {
                        $punto = new Punto();

                        $punto->fetch_by_id($row->id_punto);
                        array_push($this->puntos, $punto);
                    }

                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }

        // Funcion para comprobar si un id de juego existe en la DB
        public function id_exists($juego_id)
        {
            if (!is_null($juego_id)) {
                // Intentamos obtener el juego de la DB
                $juego_db = $this->db->get_where('juegos', array('id' => $juego_id))->last_row();

                if (!is_null($juego_db)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }


	}
?>